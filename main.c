#include "main.h"
#include "delay.h"
#include "spi.h"
#include "adc.h"
#include "iwdg.h"


RCC_ClocksTypeDef RCC_ClockFreq;
FlagStatus iwdg_state = RESET;

int main(void)
{
  RCC_HSI_Config();
  iwdg_state = RCC_GetFlagStatus(RCC_FLAG_IWDGRST);
  SysTick_Config(RCC_ClockFreq.SYSCLK_Frequency/1000);// Event 1ms
  //NVIC_SetPriority(SysTick_IRQn, 0x0);
  
  IWDG_Configuration();
  
  asm ("NOP");
  asm ("NOP");
  asm ("NOP");
  
  SPI_Configuration();
  ADC_Configuration();
  
  while(1)
  {
    asm ("NOP");
    asm ("NOP");
    
    IWDG->KR = 0xAAAA;// IWDG_ReloadCounter();
  }
}



void RCC_HSI_Config(void)
{
  RCC_DeInit();
  RCC_HSICmd(ENABLE);
  RCC_HSI48Cmd(ENABLE);
  RCC_HSEConfig(RCC_HSE_OFF);
  
  RCC_PREDIV1Config(RCC_PREDIV1_Div1);
  RCC_PLLConfig(RCC_PLLSource_HSI,  RCC_PLLMul_6);
  RCC_PLLCmd(ENABLE);
  
  FLASH_PrefetchBufferCmd(ENABLE);
  FLASH_SetLatency(FLASH_Latency_1);
  
  RCC_HCLKConfig(RCC_SYSCLK_Div1);
  RCC_PCLKConfig(RCC_HCLK_Div1);
  RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
  
  while (RCC_GetSYSCLKSource() != 0x08){/*0x08: PLL used as system clock*/}
  
  ADC_ClockModeConfig(ADC1, ADC_ClockMode_SynClkDiv4);
  RCC_ADCCLKConfig(RCC_ADCCLK_PCLK_Div4);
  
  RCC_GetClocksFreq(&RCC_ClockFreq);
}
