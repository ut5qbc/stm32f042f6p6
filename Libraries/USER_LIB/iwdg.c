#include "iwdg.h"


void IWDG_Configuration(void)
{
  RCC_LSICmd(ENABLE);
  /* Wait till LSI is ready */
  while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) != SET){}
  /* Enable Watchdog*/
  IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
  IWDG_SetPrescaler(IWDG_Prescaler_4); // IWDG_Prescaler_4 = 100us
  IWDG_SetReload(10000);// 1sec
  IWDG_ReloadCounter();
  IWDG_Enable();
}