#include "adc.h"
#include "global_config.h"
#include <stdio.h>
#include <stdlib.h>

#define BUFF_ADC        ((uint16_t)1024)
uint16_t adc_buff[BUFF_ADC] = {0};

volatile uint16_t adc_result = 0;


void DMA1_Ch1_IRQHandler(void)
{
  if (DMA1->ISR & DMA_ISR_TCIF1)
  {
    DMA1->IFCR |= DMA1_Channel1_IT_Mask;
    
    ADC1->CR |= (uint32_t)ADC_CR_ADSTP;
    /*
    bool noSwap = 0;
    
    for (int i = BUFF_ADC - 1; i >= 0; i--)
    {
      noSwap = 1;
      for (int j = 0; j < i; j++)
      {
        if (adc_buff[j] > adc_buff[j + 1])
        {
          uint16_t tmp = adc_buff[j];
          adc_buff[j] = adc_buff[j + 1];
          adc_buff[j + 1] = tmp;
          noSwap = 0;
        }
      }
      if (noSwap == 1)
        break;
    }
    */
    uint32_t mean = 0;
    
    for(uint16_t i = 0; i < BUFF_ADC-1; i++)
      mean += adc_buff[i];
    
    adc_result = mean >> 10;// mean/BUFF_ADC
  }
}

void ADC_Configuration(void)
{
  GPIO_InitTypeDef      GPIO_InitStructure;
  DMA_InitTypeDef       DMA_InitStructure;
  ADC_InitTypeDef       ADC_InitStructure;
  
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  DMA_DeInit(DMA1_Channel1);
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(ADC1->DR);
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&adc_buff;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
  DMA_InitStructure.DMA_BufferSize = count(adc_buff);
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_Init(DMA1_Channel1, &DMA_InitStructure);
  
  DMA_RemapConfig(DMA1, DMA1_CH1_ADC);
 
  ADC_DeInit(ADC1);
  ADC_StructInit(&ADC_InitStructure);
  
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_ScanDirection = ADC_ScanDirection_Upward;
  ADC_Init(ADC1, &ADC_InitStructure);
  
  ADC_ChannelConfig(ADC1, ADC_Channel_4, ADC_SampleTime_13_5Cycles);
  
  ADC_GetCalibrationFactor(ADC1);
  
  ADC_DMARequestModeConfig(ADC1, ADC_DMAMode_Circular);
  ADC_DMACmd(ADC1, ENABLE);
  
  ADC_Cmd(ADC1, ENABLE);
  while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_ADRDY));
  
  DMA_ITConfig(DMA1_Channel1, DMA1_IT_HT1, DISABLE);
  DMA_ITConfig(DMA1_Channel1, DMA1_IT_TC1, ENABLE);
  NVIC_SetPriority(DMA1_Ch1_IRQn, 0x01);
  NVIC_EnableIRQ(DMA1_Ch1_IRQn);

  ADC_StartOfConversion(ADC1);
  DMA_Cmd(DMA1_Channel1, ENABLE); 
}




