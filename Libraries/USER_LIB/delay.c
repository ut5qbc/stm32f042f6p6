#include "delay.h"


static __IO uint32_t timing_delay;

void delay(__IO uint32_t time_ms)
{ 
  timing_delay = time_ms;
  while(timing_delay != 0);
}


void delay_ms(uint32_t time_ms)
{
  uint64_t tick = (SysTick->VAL + time_ms);
  while(tick > SysTick->VAL);
}



void SysTick_Handler(void)
{
  /*
  if(timing_delay != 0)
    timing_delay--;
  */
  asm ("NOP");
}