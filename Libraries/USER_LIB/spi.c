#include "spi.h"
#include "global_config.h"

#define SPIx_SCK_PIN                     GPIO_Pin_5
#define SPIx_SCK_GPIO_PORT               GPIOA
#define SPIx_SCK_SOURCE                  GPIO_PinSource5
#define SPIx_SCK_AF                      GPIO_AF_0

#define SPIx_MISO_PIN                    GPIO_Pin_6
#define SPIx_MISO_GPIO_PORT              GPIOA
#define SPIx_MISO_SOURCE                 GPIO_PinSource6
#define SPIx_MISO_AF                     GPIO_AF_0

#define SPIx_MOSI_PIN                    GPIO_Pin_7
#define SPIx_MOSI_GPIO_PORT              GPIOA
#define SPIx_MOSI_SOURCE                 GPIO_PinSource7
#define SPIx_MOSI_AF                     GPIO_AF_0


volatile uint16_t count_bit = 0x8000;



void EXTI4_15_IRQHandler(void)
{
  if(EXTI_GetITStatus(EXTI_Line5) != RESET)
  {
    EXTI->PR = EXTI_Line5;
    
    count_bit = 0x8000;
    ADC1->CR |= (uint32_t)ADC_CR_ADSTART;
  }
  
  if(EXTI_GetITStatus(EXTI_Line6) != RESET)
  {
    EXTI->PR = EXTI_Line6;

    GPIOA->BSRR = ((adc_result & count_bit) != 0) ? GPIO_BSRR_BS_7 : GPIO_BSRR_BR_7;
    count_bit >>= 1;
  }
}

void SPI_Gpio(void)
{
  GPIO_InitTypeDef              GPIO_InitStructure;
  EXTI_InitTypeDef              EXTI_InitStructure;

  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  
  /* SPI GPIO Configuration
        PA5  ------> CS
        PA6  ------> CLK
        PA7  ------> DATA
  */
 
  GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_5|GPIO_Pin_6;// CS|CLK
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_7;// DATA
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType  = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource5);
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource6);
  
   // Configure EXTI5 line SPI_SCK
  EXTI_InitStructure.EXTI_Line = EXTI_Line5;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
  
   // Configure EXTI7 line SPI_CS
  EXTI_InitStructure.EXTI_Line = EXTI_Line6;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
  
  NVIC_SetPriority(EXTI4_15_IRQn, 0x02);
  NVIC_EnableIRQ(EXTI4_15_IRQn);
}

void SPI_Configuration(void)
{
  SPI_Gpio();
}
